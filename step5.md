<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Insert the Screw

{{BOM}}

* Insert the [screw](parts/screw.md){Qty: 1} into the block and sliding jaw.

![Insert the screw](res/step-5.svg)

