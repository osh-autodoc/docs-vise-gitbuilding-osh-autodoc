<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Documentation Vise GitBuilding / OSH Automated Documentation

Source for this [GitBuilding website](https://osh-autodoc.gitlab.io/docs-vise-gitbuilding-osh-autodoc/).

## Introduction

This project shows the Vise example from [OSH Automated
Documentation](https://osh-autodoc.org) documented with
[GitBuilding](https://gitbuilding.io) and the [FreeCAD Workbench for OSH
Automated Documentation](https://osh-autodoc.org/#_the_workbench).

The project is hosted on the [Codeberg.org OSH Automated
Documenation](https://codeberg.org/osh-autodoc/docs-vise-gitbuilding-osh-autodoc)
website, but in this particular case, we mirrored the project on
[GitLab](https://gitlab.com/osh-autodoc/docs-vise-gitbuilding-osh-autodoc)
making use of its CI for which GitBuilding can create configuration files
automatically.

## What is GitBuilding

GitBuilding is an OpenSource project for documenting hardware projects with minimal
effort, so you can stop writing and GitBuilding. GitBuilding is a python program that
works on Windows, Linux, and MacOS. More information on the GitBuilding project, or how
to install GitBuilding please see the [GitBuilding website](http://gitbuilding.io).

## What is OSH Automated Documentation

OSH Automated Documentation is software that allows you to generate
high-quality visual assembly instructions for physical products designed in CAD
software, for example high-quality assembly instructions such as for the
[Fabulaser Mini](https://www.inmachines.net/Fabulasermini) laser cutter:

![An example page generated with osh-autodoc](page-fabulaser-mini.png)

## License

Most of the files are licensed under CC-BY-SA-4.0.  See the LICENSES directory
and SPDX metadata for more details.  The project is
[REUSE](https://reuse.software/) compliant.

## Authors

- Pieter Hijma
