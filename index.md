<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Documentation Vise GitBuilding / OSH Automated Documentation

This project contains the Vise example from [OSH Automated Documentation](https://osh-autodoc.org) created by combining GitBuilding with the [FreeCAD Workbench](https://osh-autodoc.org/#_the_workbench) from OSH Automated Documentation. This page is created with Continuous Integration where the images and BOM are automatically extracted to be used in GitBuilding.

![The vise fully assemled](res/final.svg)

To assemble the vise, please follow these steps:

* [.](step1.md){step}
* [.](step2.md){step}
* [.](step3.md){step}
* [.](step4.md){step}
* [.](step5.md){step}

The [Bill of Materials]{BOM} page.
