<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

[Phillips M8 screwdriver]: Parts.yaml#Screwdriver_Philips_M8

# Attach the Fixed Jaw

{{BOM}}

* Get the [screwdriver][Phillips M8 screwdriver]{Qty: 1, Cat: tool} ready.
* Attach the [fixed jaw](parts/fixed-jaw.md){Qty: 1} with the [M8x50](parts/m8x50.md){Qty: 2} screws.

![Attach the Fixed Jaw](res/step-3.svg)

