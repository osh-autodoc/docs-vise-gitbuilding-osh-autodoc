<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Fixed Jaw

The fixed jaw of the vise.

![The fixed jaw](../res/step-3-parts/fixed-jaw.svg)
