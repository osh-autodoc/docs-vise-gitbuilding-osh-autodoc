<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# End Plate

The end plate of the sliding jaw of the vise.

![The end plate](../res/step-4.2-parts/end-plate.svg)
