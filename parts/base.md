<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Base

The base of the vise

![The base](../res/step-1-parts/base.svg)