<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Screw

The screw to slide the jaw.

![The end plate](../res/step-5-parts/screw.svg)
