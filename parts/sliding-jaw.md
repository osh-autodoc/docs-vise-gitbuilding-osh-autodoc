<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Sliding Jaw

The sliding jaw of the vise.

![The sliding jaw](../res/step-4.1-parts/sliding-jaw.svg)
