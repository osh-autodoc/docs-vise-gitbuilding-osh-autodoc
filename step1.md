<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Align the Base

{{BOM}}

* Align the [base](parts/base.md){Qty: 1} to understand how to assemble the other parts.

![Align the base](res/step-1.svg)
