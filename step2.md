<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

[Phillips M8 screwdriver]: Parts.yaml#Screwdriver_Philips_M8

# Attach the Block

{{BOM}}

* Get a [Phillips M8 screwdriver]{Qty: 1, Cat: tool} ready.
* Attach the [block](parts/block.md){Qty: 1} with the [M8x50](parts/m8x50.md){Qty: 2} screws.

![Attach the Block](res/step-2.svg)
