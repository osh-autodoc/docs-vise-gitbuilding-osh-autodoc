#!/bin/bash

# SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>
#
# SPDX-License-Identifier: Apache-2.0

function usage() {
    echo "USAGE: $(basename $0) <input dir> <FreeCAD file> <output directory>"
    echo "  The <FreeCAD file> should be given relative to the <input dir>"
    echo
}

function check_args() {
    local input_dir="$1"
    local freecad_file="$2"
    local output_dir="$3"

    if [ ! -d "$input_dir" ]
    then
	echo "$input_dir is not a directory"
	usage
	exit 1
    fi

    if [ ! -f "$input_dir"/"$freecad_file" ]
    then
	echo "$freecad_file does not exist"
	usage
	exit 1
    fi

    if [ ! -d $dir ]
    then
	echo "$output_dir is not a directory"
	usage
	exit 1
    fi
}

function call_docker() {
    local input_dir=$(realpath "$1")
    local freecad_file="$2"
    local output_dir=$(realpath "$3")

    docker run -v "$input_dir":/home/user/input -v "$output_dir":/home/user/output registry.gitlab.com/osh-autodoc/osh-autodoc-workbench:latest xvfb-run ./freecad-osh-autodoc/bin/FreeCAD "input/$freecad_file" export-svg-bom.FCMacro --pass output
}    


check_args cad/ vise-manual.FCStd res
call_docker cad/ vise-manual.FCStd res

