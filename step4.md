<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

[Phillips M8 Screwdriver]: Parts.yaml#Screwdriver_Philips_M8

# Attach the Sliding Jaw

{{BOM}}

## Align the Sliding Jaw {pagestep}

* Place the [sliding jaw](parts/sliding-jaw.md){Qty: 1}.

![Align the sliding jaw](res/step-4.1.svg)

>i **Note**
>i
>i Make sure to align with the rim:

![Align with the rim](res/step-4.1-highlight.svg)


## Attach the End Plate {pagestep}

* Use the [screwdriver][Phillips M8 Screwdriver]{Qty: 1, Cat: tool} to attach the [end plate](parts/end-plate.md){Qty: 1} to the sliding jaw with the [m8x25](parts/m8x25.md){Qty: 2} screws.


![Attach the end plate](res/step-4.2.svg)

>i **Note**
>i
>i Make sure to align with the rim:

![Align the end plate](res/step-4.2-highlight.svg)
